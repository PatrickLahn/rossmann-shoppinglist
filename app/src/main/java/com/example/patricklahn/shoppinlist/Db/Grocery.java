package com.example.patricklahn.shoppinlist.Db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Grocery {

    @Id(autoincrement = true)
    private Long Id;

    @Property
    private String name;

    @Property
    private String ean;

    @Property
    private int quantity;

    @Property
    private String picture;

    @Generated(hash = 1823643468)
    public Grocery(Long Id, String name, String ean, int quantity, String picture) {
        this.Id = Id;
        this.name = name;
        this.ean = ean;
        this.quantity = quantity;
        this.picture = picture;
    }

    @Generated(hash = 986816186)
    public Grocery() {
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEan() {
        return this.ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPicture() {
        return this.picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
